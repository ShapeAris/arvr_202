﻿using System.Collections.Generic;
using UnityEngine;

public class DetectOverlap : MonoBehaviour
{
    public static List<Collider> overlaps = new List<Collider>();

    private void Awake()
    {
        foreach (Collider c in GetComponentsInChildren<Collider>())
        {
            c.isTrigger = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Interactive")
        {
            Debug.Log("ffbiefuse");
            overlaps.Add(other);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Interactive")
        {
            overlaps.Remove(other);
        }
    }
}