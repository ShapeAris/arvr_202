﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable_Miss : Interactable
{
    Animator anim;
    public AudioClip preLaunch, Launch, explode;
    public GameObject explosion, smoke, trail;
    float secs = 2f;
    void Awake()
    {
        //Posteffects set to false during prelaunch
        explosion.SetActive(false);
        smoke.SetActive(false);
        trail.SetActive(false);
        anim = transform.GetChild(0).GetComponent<Animator>();
    }

    protected override void OnMouseDown()
    {
        Respond();
    }

    protected override void Respond()
    {
        StartCoroutine(LaunchMissile());
    }

    private IEnumerator LaunchMissile()
    {   
        //Engine starts smoke set to active with a small wait
        SoundManager.instance.PlaySingle(preLaunch);
        smoke.SetActive(true);
        
        yield return new WaitForSeconds(secs);
        
        //Launch begins trails are now active launch audio plays
        anim.SetTrigger("Launch");
        trail.SetActive(true);
        SoundManager.instance.PlaySingle(Launch);
        
        yield return new WaitForSeconds(secs);

        //Explosion particles are activated with trail and smoke now inactive with another wait to disperse particles
        SoundManager.instance.PlaySingle(explode);
        trail.SetActive(false);
        smoke.SetActive(false);
        explosion.SetActive(true);
        
        yield return new WaitForSeconds(secs);
        
        explosion.SetActive(false);
    }

}
