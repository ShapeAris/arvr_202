﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class interactableSword : Interactable
{
    public AnimationCurve animationCurve;
    Animator animator;
    public AudioClip resonance;
    public GameObject particalTrail;
    
    public AudioClip attackSound1,attackSound2;

    public float delay = 1f;
    public float secs = 2f;
    //Trail && hit inactive at idle state
    void Awake()
    {
        animator = transform.GetChild(0).GetComponent<Animator>();
        particalTrail.SetActive(false);
 
    }

    protected override void OnMouseDown() {
        Respond();
    }

    protected override void Respond()
    {
        StartCoroutine(Spun());
    }

    private IEnumerator Spun()
    {
    /*  
        begin animations and start particle trail starting with the first audioclip to a small wait to play 
        the second and turning inactive upon return
    */
        animator.SetTrigger("Deactivate");
        animator.SetTrigger("Spin");
        particalTrail.SetActive(true);
        SoundManager.instance.PlaySingle(resonance);
        
        yield return new WaitForSeconds(secs);

        SoundManager.instance.RandomizeSfx(attackSound1, attackSound2); 
        
        yield return new WaitForSeconds(delay);

        particalTrail.SetActive(false);
    
                
        yield return null;
    }
}
