﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {
	public AudioSource efxSource;
	public AudioSource musicSource;
	public static SoundManager instance = null;
	// Use this for initialization
	public float lowPitchRange = .95f;
	public float highPitchRange = 1.05f;

	void Awake () {
		if (instance == null)
		instance = this;
		else if (instance != this)
		Destroy(gameObject);
		DontDestroyOnLoad(gameObject);
	}

	public void PlaySingle (AudioClip clip) {
		efxSource.clip = clip;
		efxSource.Play ();
	}

	void Update() {
	
	}

	public void RandomizeSfx (params AudioClip [] clips) {
		int randomIndex = UnityEngine.Random.Range(0,clips.Length);
		float randomPitch = UnityEngine.Random.Range(lowPitchRange, highPitchRange);
	
		efxSource.pitch = randomPitch;
		efxSource.clip = clips[randomIndex];
		efxSource.Play();
	}

    internal void PlaySingle(AudioClip moveSound1, AudioClip moveSound2)
    {
        throw new NotImplementedException();
    }

    internal void RandomizeSfx(object titanDeath)
    {
        throw new NotImplementedException();
    }
}
