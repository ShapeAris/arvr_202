﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoVAssistant : MonoBehaviour {
    private Camera cam;

    private void OnEnable() {
        Events.OnTrackingFound += GetFoV;
    }

    private void OnDisable() {
        Events.OnTrackingFound -= GetFoV;
    }

    private void Awake() {
        cam = GetComponent<Camera>();
    }

    void GetFoV() {
        cam.fieldOfView = Camera.main.fieldOfView;
        Debug.Log(cam.fieldOfView);
    }
}
