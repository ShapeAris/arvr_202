﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable_Rocket : Interactable {
    Animator anim;
    public GameObject backSmoke,bullet,BulletHit, trail;
    public AudioClip trigger, explodeBullet;
    
    float secs = 1.5f;
    float delay = 0.5f;
    //Access animation of childs transform setting initial backsmoke to inactive
    void Awake()
    {
        anim = transform.GetChild(0).GetComponent<Animator>();
        backSmoke.SetActive(false);
        BulletHit.SetActive(false);
    }
    
    protected override void OnMouseDown()
    {
     Respond();
    }
    
    protected override void Respond()
    {
     StartCoroutine(Shoot());
    }

    private IEnumerator Shoot()
    {
        /* 
        Activate Bullet trail as animation begins while shooting short burst of smoke to indicate being fired
        for a 1.5 second wait till hit sound and explode initiates
        */

        trail.SetActive(true);
        anim.SetTrigger("shoot");
        SoundManager.instance.PlaySingle(trigger);
        backSmoke.SetActive(true);

        yield return new WaitForSeconds(secs);
        
        //Sound of HIt, trail ending to avoid uneccesary animation with another wait for the smoke to fade 
        SoundManager.instance.PlaySingle(explodeBullet);
        BulletHit.SetActive(true);
        trail.SetActive(false);
        anim.SetBool("shot", true);

        yield return new WaitForSeconds(delay);
        
     
        BulletHit.SetActive(false);
        backSmoke.SetActive(false);
    } 
}